
import React, { useState, useEffect } from 'react'
import { Line } from "react-chartjs-2"
import numeral from "numeral";



const options = {
  legend: {
    display: false,
  },
  elements: {
    point: {
      radius: 0,
    },
  },
  maintainAspectRatio: false,
  tooltips: {
    mode: "index",
    intersect: false,
    callbacks: {
      label: function (tooltipItem, data) {
        return numeral(tooltipItem.value).format("+0,0");
      },
    },
  },
  scales: {
    xAxes: [
      {
        type: "time",
        time: {
          format: "MM/DD/YY",
          tooltipFormat: "ll",
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          display: false,
        },
        ticks: {
          // Include a dollar sign in the ticks
          callback: function (value, index, values) {
            return numeral(value).format("0a");
          },
        },
      },
    ],
  },
};

function LineGraph(props) {

  const { casesType } = props
  const [data, setData] = useState({})

  const buildChartData = (data, casesType = 'cases') => {
    // console.log("imp", data)
    const chartData = []
    let lastDataPointValue;
    // console.log("imp1", data.cases) or // console.log("imp2", data[casesType])

    for (let date in data[casesType]) {
      if (lastDataPointValue) {
        chartData.push({
          x: date,
          y: data[casesType][date] - lastDataPointValue
        })
      }
      lastDataPointValue = data[casesType][date]
    }
    return chartData
  }


  useEffect(() => {
    fetch('https://disease.sh/v3/covid-19/historical/all?lastdays=120')
      .then(response => response.json())
      .then(data => {
        // console.log("line data", data)
        const chartData = buildChartData(data, casesType)
        setData(chartData)
      })
  }, [casesType])



  return (
    <div>
      {data && data.length > 0 && (
        <Line
          data={{
            datasets: [
              {
                backgroundColor: "rgba(204, 16, 52, 0.5)",
                borderColor: "#CC1034",
                data: data,
              },
            ],
          }}
          options={options}
        />
      )}

    </div>
  )
}

export default LineGraph
