import React from 'react'
import './InfoBox.css'
import { Card, CardContent, Typography } from "@material-ui/core"

function InfoBox(props) {
  const { OnClick, title, cases, total, active } = props
  return (
    <Card onClick={props.onClick} className={`infoBox ${active && "infoBox--selected"}`}>
      <CardContent>
        <Typography color="secondary"> {title}</Typography>
        <h2 className="infoBox__cases">{cases}</h2>
        <Typography className="infoBox__total" color="primary"> {total} Total</Typography>
      </CardContent>
    </Card>
  )
}

export default InfoBox
