import React, { useState, useEffect } from 'react';
import { MenuItem, FormControl, Select, Card, CardContent } from "@material-ui/core"
import './App.css'
import InfoBox from './InfoBox'
import Map from './Map'
import Table from './Table'
import { sortData } from './util'
import LineGraph from './LineGraph'
import "leaflet/dist/leaflet.css"

function App() {

  const [countries, setCountries] = useState([])
  const [country, setCountry] = useState('worldwide')
  const [countryInfo, setCountryInfo] = useState({})
  const [tableData, setTableData] = useState([])
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [mapZoom, setMapZoom] = useState(3);
  const [mapCountries, setMapCountries] = useState([])
  const [caseType, setCaseType] = useState("cases")

  // when page loads we should display  world wide values

  useEffect(() => {
    fetch("https://disease.sh/v3/covid-19/all")
      .then((response) => response.json())
      .then((data) =>
        setCountryInfo(data))
  }, [])


  // we should get the countries available

  useEffect(() => {

    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries")
        .then((response) => response.json())
        .then((data) => {
          const countries = data.map((country) => (
            {
              name: country.country,
              value: country.countryInfo.iso2,

            }
          ))
          const sorteddata = sortData(data)
          setTableData(sorteddata)
          setMapCountries(data)
          setCountries(countries)
        }
        )
    }

    getCountriesData()

  }, [])


  const onCountryChange = async (event) => {
    const countryCode = event.target.value

    const url = countryCode === "worldwide" ? "https://disease.sh/v3/covid-19/all" : `https://disease.sh/v3/covid-19/countries/${countryCode}`
    await fetch(url)
      .then(response => response.json())
      .then(data => {
        setCountry(countryCode)
        setCountryInfo(data)

        setMapCenter({
          lat: data.countryInfo.lat,
          lng: data.countryInfo.long
        })
        setMapZoom(4)
      })
  }

  // console.log("country info", country, countryInfo)
  // console.log("table data", tableData)

  return (
    <div className="app">
      <div className="app__left">
        <div className="app__header">

          <h1>Covid19 Tracker</h1>
          <h1>{country}</h1>
          <FormControl className="app__dropdown">
            <Select variant="outlined" onChange={onCountryChange} value={country} >

              <MenuItem value="worldwide">Worldwide</MenuItem>
              {
                countries.map((country) => (
                  <MenuItem value={country.value} key={country.name}>{country.name}</MenuItem>
                ))
              }

            </Select>
          </FormControl>

        </div>

        <div className="app__status">
          <InfoBox onClick={(e) => { setCaseType("cases") }} title="Coronovirus cases" cases={countryInfo.todayCases} total={countryInfo.cases} active={caseType === "cases"}></InfoBox>
          <InfoBox onClick={(e) => { setCaseType("recovered") }} title="Recovered" cases={countryInfo.todayRecovered} total={countryInfo.recovered} active={caseType === "recovered"}></InfoBox>
          <InfoBox onClick={(e) => { setCaseType("deaths") }} title="Deaths " cases={countryInfo.todayDeaths} total={countryInfo.deaths} active={caseType === "deaths"}></InfoBox>
        </div>

        {console.log("imp6", caseType)}

        <Map countries={mapCountries} casesType={caseType} center={mapCenter} zoom={mapZoom}></Map>

      </div>
      <Card className="app__right">
        <CardContent>
          <h3> Live cases by country</h3>
          <Table countries={tableData}></Table>
          <h3 className="app__mtop"> World wide new {caseType}</h3>
          <LineGraph casesType={caseType}></LineGraph>
        </CardContent>
      </Card>




    </div>
  );
}

export default App;
