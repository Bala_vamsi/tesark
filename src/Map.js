import React from 'react'
import { Map as LeafletMap, TileLayer } from "react-leaflet"
import "./Map.css"
import { showDataOnMap } from "./util"

function Map(props) {
  const { countries, casesType, center, zoom } = props
  console.log("imp7", casesType)
  console.log("imp4", countries, casesType, center, zoom)
  return (
    <div className='map'>
      <LeafletMap center={center} zoom={zoom}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />

        {showDataOnMap(countries, casesType)}
      </LeafletMap>
    </div>
  )
}

export default Map
